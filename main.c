#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include<time.h>
#include <stdbool.h>


#define BLOCK_SIZE 1048576

/*
 
 *Function to get the prints
 
 */
void printFunction(int err_code){
    
    switch(err_code){
        case 1:
            printf("Both files are empty\n");
            break;
        case 2:
            printf("The files are same\n");
            break;
            
    }
    
}

/*
 * print 16 bytes from the first difference
 */

int readDiff16bytes(int readFile1,char buffer1[BLOCK_SIZE],char buffer2[BLOCK_SIZE] ,int len)
{
    int buffSize1= len;
    int buffSize2= len;
    
    printf("\t\tfile1 \t file2\n");
    for(int i=0;i<=readFile1;i++){
        if(buffer1[i] != buffer2[i]){
            int j=i;
            
            for(j=i;j<(i+16);j++){
                // printf("buffSize1\tbuffSize2%d:%d\n",buffSize1,buffSize2);
                if(j< buffSize1 && j< buffSize2 )
                    printf("the buffer is \t%2x\t%2x\t byte:%3d\n",buffer1[j],buffer2[j],j);
                else
                    break;
            }
            while(j<i+16)
            {
                if(j<buffSize1)
                    printf("the buffer is \t\t\t%2x\t byte:%3d\n",buffer1[j],j);
                if(j<buffSize2)
                    printf("the buffer is \t\t\t%2x\t byte:%3d\n",buffer2[j],j);
                j++;
                
            }
            return 0;
            
        }
        
    }
    return 0;
}

/*
 * print 16 bytes from the first difference
 */
int print16bytes(int readFile1,char buffer2[BLOCK_SIZE])
{
    for(int k=readFile1;k<(readFile1+16);k++)
        printf("the buffer is%2x\n",buffer2[k]);
    return 0;
    
}


/*
 
 *Function to compare two hexfile which takes the file pointer of he both the files as input
 
 */

int compare_hex_files(FILE *fp1, FILE *fp2,int offset1,int offset2,int len){
    
    
    int diffOccurred =0;
    int errorCode=0;
    
    char buffer1[BLOCK_SIZE];
    char buffer2[BLOCK_SIZE];
    int  size =BLOCK_SIZE;
    int offset =0;
    
    //1st position starts from offset 0
    
    fseek ( fp1 , offset1 , SEEK_SET );//added the logic for the second code question where we need to add offset to the fle pointer
    fseek ( fp2 , offset2 , SEEK_SET );//added the logic for the second code question where we need to add offset to the fle pointer
    
    while(!feof(fp1) || !feof(fp2)){
        
        
        // int readFile1 =fread(buffer1,1,size,fp1);//commented for second code  question
        // int readFile2 =fread(buffer2,1,size,fp2);
        // offset = memcmp(buffer1,buffer2,size);
        
        int readFile1 =fread(buffer1,1,len,fp1); //size replaced by len for the second code question where we need to compare till the length =>a command line paramenter
        int readFile2 =fread(buffer2,1,len,fp2);
        offset = memcmp(buffer1,buffer2,len);
        
        
        if(offset==0)
            diffOccurred=0;
        
        else{
            
            /*
             *if the FIRST file is smaller than second file
             */
            
            if(readFile1<readFile2) {
                
                int offsetTemp =memcmp(buffer1,buffer2,readFile1);
                
                if(offsetTemp==0){//both the files are same and print the remaaining 16bytes of buffer2
                    diffOccurred=1;
                    print16bytes(readFile1,buffer2);
                    return 0;
                }
                
                if (offsetTemp!=0){//both the files are not same and print the index from  the difference point
                    diffOccurred=1;
                    readDiff16bytes(readFile1,buffer1,buffer2,len);
                    return 0;
                }
            }
            
            
            /*
             *if the second file is smaller than first file
             */
            
            if(readFile2<readFile1){
                
                int offsetTemp = memcmp(buffer1,buffer2,readFile2);
                
                if(offsetTemp==0){//both the files are same and print the remaaining 16bytes of buffer2
                    diffOccurred=1;
                    print16bytes(readFile1,buffer1);
                    return 0;
                }
                
                
                if (offsetTemp!=0){//both the files are not same and print the index from  the difference point
                    diffOccurred=1;
                    readDiff16bytes(readFile2,buffer1,buffer2,len);
                    return 0;
                }
                
            }
            
            /*
             *if the files are of same size
             */
            
            if(readFile1==readFile2){
                diffOccurred=1;
                readDiff16bytes(readFile1,buffer1,buffer2,len);
                return 0;
            }
            
        }//else loop end
    }//while loop end
    
    if(diffOccurred==0){
        errorCode=2;
        printFunction(errorCode);
    }
    
    
    return 0;
    
}

/*
 
 *main Function which takes the binary file as input and send it to the compare function
 * function to take
 
 */


int main(int argc, char** argv){
    int count;
    FILE *ptr1,*ptr2;
    
    int offsetValue1,offsetValue2,length;
    if(argc != 6){
        printf("Please enter the correct format ./main test1.bin test2.bin offsetValue1 offsetValue2 Length \n");
        return 0;
    }
    
    ptr1=fopen(argv[1],"rb");
    ptr2=fopen(argv[2],"rb");
    
    
    offsetValue1 =  atoi(argv[3]);
    offsetValue2 =  atoi(argv[4]);
    length       =  atoi(argv[5]);
    
    
    if (ptr1 == NULL){
        printf("Unable to open file %s\n",argv[1]);
        return 0;
    }
    
    if (ptr2 == NULL){
        printf("Unable to open file %s\n",argv[2]);
        return 0;
    }
    
    clock_t begin =clock();//Start the clock timer
    compare_hex_files(ptr1, ptr2,offsetValue1,offsetValue2,length);//Call the function
    clock_t end =clock();//End the Clock timer
    
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Time spent %lf\n",time_spent);
    
    
    return 0;
}

